# A Docker Workspace made for use at Oak Digital

## What is it?

This project is aiming to create a docker based workspace for local web development, specifically
with wordpress in mind. It uses the
[caddy-docker-proxy](https://github.com/lucaslorentz/caddy-docker-proxy) as a reverse proxy as well
as for serving `php-fpm` containers.

It is still in development, as the goal is to make use of this envirnoment as easy and quick as
possible, I want it to be set and forget so we have to waste as little time as possible configuring
servers.

## What is included?

The workspace are at the moment coming with following services:

-   caddy - Caddy is the reverse proxy gluing the whole thing together
-   db - A container with MariaDB, for use with wordpress
-   mailhog - A mailtrap for managing mail on local servers
-   portainer (disabled as default) - For development purposes it is nice to have easy access to all your docker containers.
-   phpmyadmin (disabled as default) - PHPMyAdmin for inspecting and managing the database

A simple launch script that will run the docker-compose up command. _NB.: Remember to rename the `.env.dist` files to `.env` and fill out the needed information before running._

## Structure

The structure is inspired by Pascal Landau and his
[great docker tutorial](https://www.pascallandau.com/blog/structuring-the-docker-setup-for-php-projects/#structuring-the-repository).

### The `.docker` folder

The workspace is structured such that all docker related files are located in the `.docker` folder,
containing the main `docker-compose.yaml`, a `.shared` folder for any resources shared between
containers and then a dedicated folder for each service that needs one.

### Workfolders

Like in the traditional www folder the root folder acts as server root, that means caddy has access
to it which enables it to serve the files for the fpm servers.

When you add a new site, the source folder should be added in the root folder (the folder containing
the `.docker` folder).

## Setting up a new site

See [this repo](https://bitbucket.org/davkochg/oak-wordpress/src/beta/) for more info on how to setup local wordpress sites.
