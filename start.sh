#! /usr/bin/env sh

docker-compose \
    --file .config/docker-compose.yaml \
    --env-file .config/.env \
    -p Workspace\
    up -d
